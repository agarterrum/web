<?php

namespace app\models;

use Yii;
use yii\db\Query;
use app\models\Metric_value;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "metric".
 *
 * @property int $metric_id
 * @property string $metric_name
 * @property string $descr
 * @property int $min
 * @property int $max
 * @property int $server_id
 *
 * @property Server $server
 * @property MetricValue[] $metricValues
 */
class Metric extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metric';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['metric_name', 'min', 'max', 'server_id'], 'required'],
            [['descr'], 'string'],
            [['min', 'max', 'server_id'], 'integer'],
            [['metric_name'], 'string', 'max' => 255],
            [['metric_name'], 'unique'],
            [['server_id'], 'exist', 'skipOnError' => true, 'targetClass' => Server::className(), 'targetAttribute' => ['server_id' => 'server_id']],
            ['metric_name', 'validateName']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'metric_id' => 'Metric ID',
            'metric_name' => 'Metric Name',
            'descr' => 'Descr',
            'min' => 'Min',
            'max' => 'Max',
            'server_id' => 'Server ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServer()
    {
        return $this->hasOne(Server::className(), ['server_id' => 'server_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetricValues()
    {
        return $this->hasMany(MetricValue::className(), ['metric_id' => 'metric_id']);
    }
    public function fields()
    {
        return [
            'metric_id',
            'metric_name',
            'descr',
            'min',
            'max',
            'server_id',
            'last'
        ];
    }
    public function getLast()
    {
        $query = Metric_value::find()->select('value')->where(['metric_id' => $this->metric_id])->orderBy('time DESC')->one();
        if(!empty($query))
            return $query->value;
        else
            return NULL;
    }
    public function validateName(){
        if (!$this->hasErrors()){
            $server = $this->getServer()->one();
            $query = Metric::find()->select('metric_name')->where(['server_id' => $server])->andWhere(['metric_name'=> $this->metric_name])->one();

            if(!empty($query)){
                throw new ForbiddenHttpException('This metric already belongs to the server.');
            }
        }
    }
}
