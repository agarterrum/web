<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "metric_value".
 *
 * @property int $value_id
 * @property double $value
 * @property string $time
 * @property int $metric_id
 *
 * @property Metric $metric
 */
class Metric_value extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metric_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value', 'metric_id'], 'required'],
            [['value'], 'number'],
            [['time'], 'safe'],
            [['metric_id'], 'integer'],
            [['metric_id'], 'exist', 'skipOnError' => true, 'targetClass' => Metric::className(), 'targetAttribute' => ['metric_id' => 'metric_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'value_id' => 'Value ID',
            'value' => 'Value',
            'time' => 'Time',
            'metric_id' => 'Metric ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetric()
    {
        return $this->hasOne(Metric::className(), ['metric_id' => 'metric_id']);
    }
    public function fields()
    {
        return ['value_id', 'value', 'time', 'metric_id'];
    }
}
