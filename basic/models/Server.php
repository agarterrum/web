<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "server".
 *
 * @property int $server_id
 * @property string $server_name
 * @property string $descr
 *
 * @property Metric[] $metrics
 */
class Server extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'server';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['server_name'], 'required'],
            [['descr'], 'string'],
            [['server_name'], 'string', 'max' => 255],
            [['server_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'server_id' => 'Server ID',
            'server_name' => 'Server Name',
            'descr' => 'Descr',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetrics()
    {
        return $this->hasMany(Metric::className(), ['server_id' => 'server_id']);
    }

    public function fields()
    {
        return ['server_id', 'server_name', 'descr'];
    }
}
