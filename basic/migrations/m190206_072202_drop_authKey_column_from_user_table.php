<?php

use yii\db\Migration;

/**
 * Handles dropping authKey from table `user`.
 */
class m190206_072202_drop_authKey_column_from_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user', 'authKey');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('user', 'authKey');
    }
}
