<?php

use yii\db\Migration;

/**
 * Class m190206_072647_access_token_can_be_null
 */
class m190206_072647_access_token_can_be_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user', 'accessToken', $this->string()->null());
        $this->alterColumn('user', 'password', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('user', 'accessToken', $this->string()->notNull());
        $this->alterColumn('user', 'password', $this->string()->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190206_072647_access_token_can_be_null cannot be reverted.\n";

        return false;
    }
    */
}
