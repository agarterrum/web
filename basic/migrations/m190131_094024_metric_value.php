<?php

use yii\db\Migration;

/**
 * Class m190131_094024_metric_value
 */
class m190131_094024_metric_value extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('metric_value', [
            'value_id'=>$this->primaryKey(),
            'value'=>$this->float()->notNull(),
            'time'=>$this->timestamp()->notNull(),
            'metric_id'=>$this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-metric_value-metric_id',
            'metric_value',
            'metric_id'
        );

        $this->addForeignKey(
            'fk-metric_value-metric_id',
            'metric_value',
            'metric_id',
            'metric',
            'metric_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-metric_value-metric_id',
            'metric_value'
        );

        $this->dropIndex(
            'idx-metric_value-metric_id',
            'metric_value'
        );

        $this->dropTable('metric_value');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190131_094024_metric_value cannot be reverted.\n";

        return false;
    }
    */
}
