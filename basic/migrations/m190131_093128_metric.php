<?php

use yii\db\Migration;

/**
 * Class m190131_093128_metric
 */
class m190131_093128_metric extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('metric', [
            'metric_id' =>$this->primaryKey(),
            'metric_name'=>$this->string()->notNull(),
            'descr'=>$this->text()->notNull(),
            'min'=>$this->integer()->notNull(),
            'max'=>$this->integer()->notNull(),
            'server_id'=>$this->integer()->notNull(),
        ]);
        $this->createIndex(
            'idx-metric-server_id',
            'metric',
            'server_id'
        );
        $this->addForeignKey(
            'fk-metric-server_id',
            'metric',
            'server_id',
            'server',
            'server_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-metric-server_id',
            'metric'
        );
        $this->dropIndex(
            'idx-metric-server_id',
            'metric'
        );
        $this->dropTable("metric");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190131_093128_metric cannot be reverted.\n";

        return false;
    }
    */
}
