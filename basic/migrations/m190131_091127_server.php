<?php

use yii\db\Migration;

/**
 * Class m190131_091127_server
 */
class m190131_091127_server extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('server', [
            'server_id' => $this->primaryKey(),
            'server_name' => $this->string()->notNull()->unique(),
            'descr'=>$this->text()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('server');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190131_101834_server cannot be reverted.\n";

        return false;
    }
    */
}
