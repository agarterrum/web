<?php

use yii\db\Migration;

/**
 * Class m190204_174239_add_test_data
 */
class m190204_174239_add_test_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('server', ['server_name', 'descr'],
            [['server.id1', 'Первый сервер'],['server.id2', 'Второй сервер'],['server.id3', 'Третий сервер'],
                ['server.id4', 'Четвертый сервер'],['server.id5', 'Пятый сервер']]);

        $this->batchInsert('metric', ['metric_name','descr','min','max','server_id'],[
            ['cpu.average','Средняя загруженность всех процесссоров', 0, 100, 1],
            ['ram.average','Средняя загруженность оперативной памяти', 0, 100, 1],
            ['cpu.average','Средняя загруженность всех процесссоров', 0, 100, 2],
            ['ram.average','Средняя загруженность оперативной памяти', 0, 100, 2],
            ['cpu.average','Средняя загруженность всех процесссоров', 0, 100, 3],
            ['ram.average','Средняя загруженность оперативной памяти', 0, 100, 3],
            ['cpu.average','Средняя загруженность всех процесссоров', 0, 100, 4],
            ['ram.average','Средняя загруженность оперативной памяти', 0, 100, 4],
            ['cpu.average','Средняя загруженность всех процесссоров', 0, 100, 5],
            ['ram.average','Средняя загруженность оперативной памяти', 0, 100, 5]
        ]);
        $this->batchInsert('metric_value', ['value','time','metric_id'], [
            [10, '2019-01-04 23:34:12', 1], [20, '2019-01-04 23:34:13', 1], [30, '2019-01-04 23:34:14', 1], [40, '2019-01-04 23:34:15', 1],
            [50, '2019-01-04 23:34:12', 2], [60, '2019-01-04 23:34:13', 2], [70, '2019-01-04 23:34:14', 2], [80, '2019-01-04 23:34:15', 2],
            [90, '2019-01-04 23:34:12', 3], [10, '2019-01-04 23:34:13', 3], [20, '2019-01-04 23:34:14', 3], [30, '2019-01-04 23:34:15', 3],
            [40, '2019-01-04 23:34:12', 4], [50, '2019-01-04 23:34:13', 4], [60, '2019-01-04 23:34:14', 4], [70, '2019-01-04 23:34:15', 4],
            [80, '2019-01-04 23:34:12', 5], [90, '2019-01-04 23:34:13', 5], [10, '2019-01-04 23:34:14', 5], [20, '2019-01-04 23:34:15', 5],
            [30, '2019-01-04 23:34:12', 6], [40, '2019-01-04 23:34:13', 6], [50, '2019-01-04 23:34:14', 6], [60, '2019-01-04 23:34:15', 6],
            [70, '2019-01-04 23:34:12', 7], [80, '2019-01-04 23:34:13', 7], [90, '2019-01-04 23:34:14', 7], [10, '2019-01-04 23:34:15', 7],
            [20, '2019-01-04 23:34:12', 8], [30, '2019-01-04 23:34:13', 8], [40, '2019-01-04 23:34:14', 8], [50, '2019-01-04 23:34:15', 8],
            [60, '2019-01-04 23:34:12', 9], [70, '2019-01-04 23:34:13', 9], [80, '2019-01-04 23:34:14', 9], [90, '2019-01-04 23:34:15', 9],
            [10, '2019-01-04 23:34:12',10], [20, '2019-01-04 23:34:13',10], [30, '2019-01-04 23:34:14',10], [40, '2019-01-04 23:34:15',10]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190204_174239_add_test_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190204_174239_add_test_data cannot be reverted.\n";

        return false;
    }
    */
}
