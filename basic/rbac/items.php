<?php
return [
    'index' => [
        'type' => 2,
    ],
    'view' => [
        'type' => 2,
    ],
    'update' => [
        'type' => 2,
    ],
    'delete' => [
        'type' => 2,
    ],
    'create' => [
        'type' => 2,
    ],
    'operator' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'index',
            'view',
        ],
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'delete',
            'update',
            'create',
            'operator',
        ],
    ],
];
