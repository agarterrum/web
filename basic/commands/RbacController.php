<?php

namespace app\commands;

use Yii;
use \app\rbac\UserGroupRule;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $authManager = Yii::$app->authManager;

        // Create roles
        $operator  = $authManager->createRole('operator');
        $admin  = $authManager->createRole('admin');

        // Create simple, based on action{$NAME} permissions
        $index  = $authManager->createPermission('index');
        $view   = $authManager->createPermission('view');
        $update = $authManager->createPermission('update');
        $delete = $authManager->createPermission('delete');
        $create = $authManager->createPermission('create');

        // Add permissions in Yii::$app->authManager
        $authManager->add($index);
        $authManager->add($view);
        $authManager->add($update);
        $authManager->add($delete);
        $authManager->add($create);


        // Add rule, based on UserExt->group === $user->group
        $userGroupRule = new UserGroupRule();
        $authManager->add($userGroupRule);

        // Add rule "UserGroupRule" in roles
        $operator->ruleName  = $userGroupRule->name;
        $admin->ruleName  = $userGroupRule->name;

        // Add roles in Yii::$app->authManager
        $authManager->add($operator);
        $authManager->add($admin);

        // Add permission-per-role in Yii::$app->authManager

        // Operator
        $authManager->addChild($operator, $index);
        $authManager->addChild($operator, $view);

        // Admin
        $authManager->addChild($admin, $delete);
        $authManager->addChild($admin, $update);
        $authManager->addChild($admin, $create);
        $authManager->addChild($admin, $operator);
    }
}
