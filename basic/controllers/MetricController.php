<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\IndexAction;
use yii\web\ForbiddenHttpException;


class MetricController extends ActiveController
{
    public $modelClass = 'app\models\Metric';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index'] = [
            'class' => IndexAction::class,
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'dataFilter' => [
                'class' => 'yii\data\ActiveDataFilter',
                'searchModel' => function () {
                    return (new \yii\base\DynamicModel(['server_id' => null]))
                        ->addRule('server_id', 'integer');
                },
            ],
        ];
        return $actions;
    }

    public function actionTest()
    {
        echo "Выполнилась тестовая функция.\n";
        die();
    }


    public function checkAccess($action, $model = null, $params = [])
    {
        if (!Yii::$app->user->can($action)) {
            throw new ForbiddenHttpException('Access denied');
        }
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
        ];

        return $behaviors;
    }

}
