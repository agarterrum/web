<?php

namespace app\controllers;

use yii\rest\Controller;
use app\models\Server;
use app\models\Metric;
use app\models\Metric_value;
use yii\web\ServerErrorHttpException;

class SocketController extends Controller
{
    public function actionTakeMetric(){

        $socket_url = "http://web/pub";

        $request = \Yii::$app->request;
        # для записи принимаем только POST запрос
        if ($request->isPost){
            # todo заглушка для резолвинга IP УДАЛИТЬ
            $server_name = "";
            if ("172.20.0.1" == $request->userIP)
            {
                $server_name = "server.id2";
                #echo "это локальный хост!";
            } else {
                $server_name = gethostbyaddr($request->userIP);
            }
            # проверяем, есть ли данный сервер с списке тех, от кого мы принимаем метрики
            $server = Server::getDb()->cache(function () use($server_name){
               return Server::findOne(['server_name' => $server_name]);
            });
            if (!empty($server)) {
                $server_id = $server->server_id;
                $request_data = explode(" ", $request->getRawBody());
                $metric_name = $request_data[0];
                # получаем id метрики
                $metric_raw = Metric::getDb()->cache(function ($db) use($metric_name, $server_id){
                    return Metric::findOne(['metric_name' => $metric_name, 'server_id' => $server_id]);
                });
                $metric_id = $metric_raw->metric_id;
                # вводим полученные значений в бд
                $metric_value = new Metric_value();
                $metric_value->metric_id = $metric_id;
                $metric_value->value = $request_data[1];
                // преобразование даты из локального времени в UTC (костыль)
                $date = $request_data[2] + 3600*5;
                $metric_value->time = date('Y-m-d H:i:s', $date);
                if($metric_value->save()) {
                    # отправляем json с данными этой метрики
                    $array = array('metric_name' => $metric_name, 'metric_id' => $metric_id, 'server_name' => $server_name, 'value' => $request_data[1], 'time' => date('Y-m-d H:i:s', $request_data[2]));
                    $json_output = json_encode($array);
                    $ch = curl_init($socket_url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_output);
                    curl_exec($ch);
                    curl_close($ch);
                } else {
                    throw new ServerErrorHttpException();
                }
            } else {
                echo "Ваш ip: ";
                var_dump($request->userIP);
                echo "Ошибка. Данный сервер не отслеживается, либо указан неправильный локальный хост\n";
            }

        }
    }
}
