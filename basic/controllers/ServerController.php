<?php

namespace app\controllers;

use app\models\Server;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;


class ServerController extends ActiveController
{
    public $modelClass = 'app\models\Server';

    public function checkAccess($action, $model = null, $params = [])
    {
        if (!Yii::$app->user->can($action)) {
            throw new ForbiddenHttpException('Access denied');
        }
    }
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']=[
            'class' => HttpBearerAuth::class,
        ];

        return $behaviors;
    }

    public function actions(){
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }
    public function actionIndex()
    {
        return new ActiveDataProvider([
            'query' => Server::find(),
            'pagination' => false,
        ]);
    }
};
