<?php
    namespace app\controllers;

    use Yii;
    use yii\filters\auth\HttpBearerAuth;
    use yii\rest\ActiveController;
    use yii\web\ForbiddenHttpException;

    class MetricValueController extends ActiveController
    {
        public $modelClass = 'app\models\Metric_value';

        public function actions()
        {
            $actions = parent::actions();

            $actions['index'] = [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'dataFilter' => [
                    'class' => 'yii\data\ActiveDataFilter',
                    'searchModel' => function () {
                        return (new \yii\base\DynamicModel(['metric_id' => null, 'time' => null]))
                            ->addRule('metric_id', 'integer')
                            ->addRule('time', 'date', ['format' => 'php:Y-m-d H:i:s']);
                    },
                ]
            ];
            return $actions;
        }
        public function checkAccess($action, $model = null, $params = [])
        {
            if (!Yii::$app->user->can($action)) {
                throw new ForbiddenHttpException('Access denied');
            }
        }
        public function behaviors()
        {
            $behaviors = parent::behaviors();
            $behaviors['authenticator']=[
                'class' => HttpBearerAuth::class,
            ];

            return $behaviors;
        }
    }
