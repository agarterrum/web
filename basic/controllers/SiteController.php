<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\rest\Controller;
use yii\web\Response;
use app\models\LoginForm;
use yii\filters\auth\HttpBearerAuth;

class SiteController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'only' => ['logout']
        ];
        return $behaviors;
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->login()) {
            // возвращаем токен
            return ['accessToken' => $model->getUser()->accessToken];
        }
        return $model;
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;
        if (!$user->removeAccessToken()) {
            return $user;
        }
        return [];
        //todo сделать работающий логаут
    }
}
