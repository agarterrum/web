<?php
    return [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=db;dbname=metrics',
        'username' => 'user',
        'password' => 'user_pw',
        'charset' => 'utf8',
        'enableSchemaCache' => true,
        'schemaCacheDuration' => 60,
        'schemaCache' => 'cache',
        // Schema cache options (for production environment)
        //'enableSchemaCache' => true,
        //'schemaCacheDuration' => 60,
        //'schemaCache' => 'cache',
    ];
