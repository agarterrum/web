import React from "react";
import ReactDOM from "react-dom";
import Chart from "./components/Chart";
import {SiteHeader} from "./components/Navigation";
import {Sidebar} from "./components/Sidebar";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            servers: {},
            metrics: {},
            serverTarget: 0,
            history: [],
            real: {},
            realTime: [],
            token: "",
            metricsID: 0,
            graphics: {
                x: "",
                y: ""
            },
            chart: [],
            metric_names: ""
        };
        this.serverTarget = this.serverTarget.bind(this);
        this.getToken = this.getToken.bind(this);
        this.serverList = this.serverList.bind(this);
        this.logOutHandler = this.logOutHandler.bind(this);
        this.graphMetrID = this.graphMetrID.bind(this);
    }

    componentDidMount() {
        const socket = new WebSocket("ws://192.168.14.228:80/sub");
        socket.onmessage = function (message) {
            let data;
            try {
                data = JSON.parse(message.data);
            } catch (error) {
                console.log(error);
                return;
            }
            const metricState = Object.assign({}, this.state.metrics);
            const graphicState = Object.assign({}, this.state.graphics);
           // const chartState = this.state.chart;
            if (parseInt(data.metric_id) == this.state.metricsID) {
                graphicState.y=parseInt(data.value);
                graphicState.x=new Date(data.time);
               // chartState.push(graphicState).splice(0,2);

                this.setState({
                    graphics: graphicState,
                    chart: [graphicState].concat(this.state.chart).splice(0,10)
                });
                //console.log(this.state.chart);
            }

            // data = {
            //     "metric_name": "ram.average",
            //     "metric_id": 2,
            //     "server_name": "server.id1",
            //     "value": "112",
            //     "time": "2019-01-29 09:12:57"
            // }
            if (metricState.hasOwnProperty(data.metric_id)) {
                metricState[data.metric_id] = data;
                this.setState({
                    metrics: metricState,
                    history: [data].concat(this.state.history).splice(0, 10)
                });
                //console.log(this.state.history);
            }
        }.bind(this);
    }

    /*Получаем metric_id при нажатии на кнопку в Sidebar*/
    graphMetrID(id, metric_names) {
       // console.log(id);
        this.setState({
            metricsID: id,
            chart: [],
            metric_names: metric_names
        });
    }

    /* Обнуление страницы при Logout */
    logOutHandler() {
        alert("Вы успешно вышли из аккаунта");
        this.setState({
            servers: {},
            metrics: {},
            token: "",
            chart: [],
            history: [],
            metricsID: 0
        });
    }

    /*Посылаем запрос на получение списка серверов при нажатии на кнопку "Получить" в Sidebar*/
    serverList() {
        if (this.state.token) {
            fetch("/servers", {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${this.state.token}`
                }
            })
                .then(function (response) {
                 //   console.log(response.headers.get("X-Pagination-Page-Count"));
                    if (response.status >= 200 && response.status < 300) {
                        return response;
                    }
                })

                .then(response => response.json())
                .then(response => {
                    const serversState = Object.assign({}, this.state.servers);
                    response.forEach(item => {
                        // 1: {name: "server1", description: "SERVER1", server_id: 1},
                        // 2: {name: "server2", description: "SERVER2", server_id: 2}
                        serversState[item.server_id] = item;
                    });
                    this.setState({servers: serversState});
                });
        }
    }

    /*По нажатию на один из серверов в списке, функция получает его id и выводит последние актуальные метрики*/
    serverTarget(server_id) {
        if (server_id !== this.state.serverTarget) {

            //fetch - последние метрики

            fetch(`/metrics?filter[server_id]=${server_id}`, {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${this.state.token}`
                }
            })
                .then(response => response.json())
                .then(response => {
                    const metrics = {};
                    response.forEach(item => {
                        //{1: {}, 10 : {}, 27 : {}}
                        metrics[item.metric_id] = {value: item.last, time: 0, metric_name: item.metric_name};
                    });
                    this.setState({
                        serverTarget: server_id,
                        metrics: metrics,
                        history: [],
                        chart: [],
                        metricsID: 0
                    });
                });
        }
    }

    /*Функция получает токен при авторизации пользователя*/
    getToken(token) {
        this.setState({
            token: token
        });
    }

    render() {
        return <div>
            <SiteHeader getToken={this.getToken} token={this.state.token} logOutHandler={this.logOutHandler}/>
            <Sidebar servers={this.state.servers} onServerTarget={this.serverTarget} metrics={this.state.metrics}
                     serverList={this.serverList} count={this.state.count}
                     graphMetrID={this.graphMetrID}/>
            <Chart history={this.state.history} metricID={this.state.metricID} datasPoints={this.state.chart} metric_name={this.state.metric_names}/>
        </div>;
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById("root")
);
