import React from "react";
import ReactDOM from "react-dom";

//import {Sidebar} from "./components/Sidebar";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            servers: {},
            metrics: {},
            serverTarget: 0
        };
        this.serverTarget = this.serverTarget.bind(this);
    }

    componentDidMount() {
        const socket = new WebSocket("ws://localhost:8080/sub");
        socket.onmessage = function (message) {
            let data;
            try {
                data = JSON.parse(message.data);
            } catch (error) {
                console.log(error);
                return;
            }
            const metricState = Object.assign({}, this.state.metrics);
            // {
            //     "metric_name": "ram.average",
            //     "metric_id": 2,
            //     "server_name": "server.id1",
            //     "value": "112",
            //     "time": "2019-01-29 09:12:57"
            // }
            if (metricState.hasOwnProperty(data.metric_id)){
                metricState[data.metric_id] = data;
                this.setState({metrics : metricState});
            }
        }.bind(this);

        fetch("/servers", {method: "GET"})
            .then(response => response.json())
            .then(response => {
                const serversState = Object.assign({}, this.state.servers);
                response.forEach(item => {
                    // 1: {name: "server1", description: "SERVER1", server_id: 1},
                    // 2: {name: "server2", description: "SERVER2", server_id: 2}
                    serversState[item.server_id] = item;
                });
                this.setState({servers: serversState});
            });
    }
    serverTarget(server_id){
        if (server_id !== this.state.serverTarget) {
            fetch(`/metrics?filter[server_id]=${server_id}`)
                .then(response => response.json())
                .then(response => {
                    console.log(response);
                    const metrics = {};
                    response.forEach(item => {
                        //{1: {}, 10 : {}, 27 : {}}
                        metrics[item.metric_id] = {value: item.last, time: 0, metric_name: item.metric_name};
                    });
                    this.setState({
                        serverTarget: server_id,
                        metrics: metrics
                    });
                });
        }
    }
    render() {
        return <div>
            <Sidebar servers={this.state.servers} onServerTarget={this.serverTarget}/>
            <MainBlock metrics={this.state.metrics}/>
        </div>;
    }
}

class Sidebar extends React.Component {
    constructor(props){
        super(props);
        this.handleClick=this.serverClick.bind(this);
    }
    serverClick(server_id) {
        if (this.props.onServerTarget) {
            this.props.onServerTarget(parseInt(server_id));
        }
    }
    render() {
        return <ul>
            {
                Object.keys(this.props.servers).map(id => {
                    return <li key={id}><a href="javascript:void(0);" onClick={() => {this.handleClick(id)}}>{this.props.servers[id]["server_name"]}</a></li>;
                })
            }
        </ul>;
    }
}

class MainBlock extends React.Component {
    render() {
        return <ul>
            {
                Object.keys(this.props.metrics).map(id => {
                    return <li key={id}>{this.props.metrics[id]["metric_name"] + " - " + this.props.metrics[id]["value"]}</li>;
                })
            }
        </ul>;
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById("root")
);
