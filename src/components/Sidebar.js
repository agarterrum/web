import React, { Component } from "react";
import "./../styles/App.css";

export class Sidebar extends React.Component {
    constructor(props){
        super(props);
        this.handleClick=this.serverClick.bind(this);
        this.serversList=this.serversList.bind(this);
    //    this.forceUpdateSidebar=this.forceUpdateSidebar.bind(this);
    }
    serverClick(server_id) {
        if (this.props.onServerTarget) {
            this.props.onServerTarget(parseInt(server_id));
        }
    }

    serversList (){
        this.props.serverList();
    }

    // forceUpdateSidebar() {
    //     if (this.props.count == 1){
    //         this.forceUpdate();
    //     }
    // }



    render() {
        return <nav className="col-md-2 d-none d-md-block bg-light sidebar">
            <div className="sidebar-sticky">
                <br/>
                <br/>
                <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>Список серверов</span>
                    <a className="d-flex align-items-center text-muted" href="#">
                        <button className="btn btn-sm btn-outline-secondary" onClick={this.serversList}>Получить</button>
                    </a>
                </h6>
                <br />
                <div className="list-group valera">
                    {
                        Object.keys(this.props.servers).map(id => {
                            return <a className="list-group-item list-group-item-action text-md-center" key={`valss_${id}`} href="javascript:void(0);" onClick={() => {this.handleClick(id)}}>{this.props.servers[id]["server_name"]}</a>;
                        })
                    }
                </div>

                <br/>

                <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted text-center">
                    <span className="text-md-center">Текущие значения метрик</span>
                    <a className="d-flex align-items-center text-muted" href="#">
                        <span data-feather="plus-circle"></span>
                    </a>
                </h6>

                <br/>
                <div className="valera2">
                    <table className="table table-striped table-sm">
                        <thead>
                            <tr className="text-center">
                                <th>Метрика</th>
                                <th>Значение</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                <React.Fragment>

                                    <SidebarRowMetric metrics={this.props.metrics} graphMetrID={this.props.graphMetrID}/>

                                </React.Fragment>
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </nav>;
    }
}

class SidebarRowMetric extends React.Component {
    constructor(props){
        super(props);
        this.graphMetricsID=this.graphMetricsID.bind(this);
    }

    graphMetricsID (id, metric_name){
        this.props.graphMetrID(id, metric_name);
    }
    render() {
        return Object.keys(this.props.metrics).map(id => {
            return <React.Fragment>
                <tr>
                    <td key={`name_${id}`}><a href="#" className="list-group-item list-group-item-action text-md-center" onClick={() => {this.graphMetricsID(id, this.props.metrics[id]["metric_name"])}}>{this.props.metrics[id]["metric_name"]}</a></td>
                    <td key={`val_${id}`}><li className="list-group-item list-group-item-action text-md-center">{this.props.metrics[id]["value"]}</li></td>
                </tr>
            </React.Fragment>;
            // return <td key={id}>{this.props.metrics[id]["metric_name"] + " - " + this.props.metrics[id]["value"]}</td>;
        });


    }
}


