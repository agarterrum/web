/* App.js */
var React = require('react');
var Component = React.Component;
var CanvasJSReact = require('./canvasjs.react.js');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
class ChartView extends Component {
    render() {
        const opt = {
            animationEnabled: true,
            exportEnabled: true,
            theme: "light2", // "light1", "dark1", "dark2"
            title:{
                text: `График нагрузки по метрике ${this.props.metric_name}`
            },
            axisY: {
                title: "Значение",
                includeZero: false,
                //suffix: "%"

            },
            axisX: {
                title: "Время",
                //prefix: "W",
                // interval: 2,
                labelFormatter: function (e) {
                    return CanvasJS.formatDate( e.value, "HH:mm:ss");
                },
            },
            data: [{
                type: "line",
                toolTipContent: "{x}: {y}",
                dataPoints: this.props.datasPoints
            }]
        };
        return (
            <div>
                <CanvasJSChart options = {opt}
                    /* onRef={ref => this.chart = ref} */
                />
                {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
            </div>
        );
    }
}

class Chart extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const $this=this;
        return <main role="main" className="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div
                className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h1 className="h2">Общая нагрузка на сервер</h1>
                <div className="btn-toolbar mb-2 mb-md-0">
                    <div className="btn-group mr-2">
                        <button className="btn btn-sm btn-outline-secondary">Скачать</button>
                        <button className="btn btn-sm btn-outline-secondary">Поделиться</button>
                    </div>
                    <button className="btn btn-sm btn-outline-secondary dropdown-toggle">
                        <span data-feather="calendar"></span>
                        Эта неделя
                    </button>
                </div>
            </div>

            {/*<div id="chartContainer"></div>*/}

            {
                <ChartView metricID={this.props.metricID} datasPoints={this.props.datasPoints} metric_name={this.props.metric_name}/>
            }

            <h2>Метрики сервера в реальном времени</h2>
            <div className="table-responsive">
                {
                    this.props.history.length > 0 ?
                        <table className="table table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>Метрика</th>
                                    <th>Идентификатор метрики</th>
                                    <th>Имя сервера</th>
                                    <th>Значение</th>
                                    <th>Время</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.props.history.map(row => {
                                        return <tr key={`row_${row}`}>
                                            {
                                                Object.keys(row).map(col => {
                                                    //console.log(col);
                                                    if (parseInt(row.value) >100) {
                                                        return <td key={col} className="bg-danger">{row[col]}</td>;
                                                    } else {
                                                        return <td key={col} className="bg-success">{row[col]}</td>;
                                                    }

                                                })
                                            }
                                        </tr>;
                                    })
                                }
                            </tbody>
                        </table>
                        : ''
                }
            </div>
        </main>;
    }
}

module.exports = Chart;
