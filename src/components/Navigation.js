import React from "react";

export class SiteHeader extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        if (!this.props.token) {
            return <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
                <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="#">IntersvyazMetricSystem</a>
                {
                    <AuthForm getToken={this.props.getToken}/>
                }
            </nav>;
        } else {
            return <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
                <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="#">IntersvyazMetricSystem</a>
                {
                    <ExitForm token={this.props.token} logOutHandler={this.props.logOutHandler}/>
                }
            </nav>;
        }
    }
}

class AuthForm extends React.Component {
    constructor(props) {
        super(props);
        this.alertFunction=this.alertFunction.bind(this);
    }

    alertFunction = event => {
        event.preventDefault();
        let data = new FormData();
        data.append('username',this.loginInput.value);
        data.append('password',this.passInput.value);
        fetch("/site/login", {
            method: "POST",
            body: data
        })
            .then((res) => res.json())
            .then((data) => {
                if (data.hasOwnProperty("accessToken")){
                   // alert("Успешная регистрация");
                    Object.values(data).map(token =>{
                        this.props.getToken(token);
                    });
                } else {
                    alert("Неверный логин или пароль");
                }
            });
    };
    render() {
        return <form className="form-inline" onSubmit={this.alertFunction}>
            <div className="form-group">
                <label className="sr-only" htmlFor="exampleInputEmail3">Логин</label>
                <input type="text" className="form-control" id="exampleInputEmail3" placeholder="Логин" name="username" ref={e => this.loginInput = e}/>
            </div>
            <div className="form-group">
                <label className="sr-only" htmlFor="exampleInputPassword3">Пароль</label>
                <input type="password" className="form-control" id="exampleInputPassword3" placeholder="Пароль" name="password" ref={e => this.passInput = e}/>
            </div>
            <button type="submit" className="btn btn-secondary" value="submit">Войти</button>
        </form>;
    }
}

class ExitForm extends React.Component {
    constructor(props){
        super(props);
        this.logOut=this.logOut.bind(this);
    }

    logOut() {
        fetch("/site/logout", {
            method: "GET",
            headers: {
                Authorization: `Bearer ${this.props.token}`
            }
        })
            .then((res) => res.json());
        this.props.logOutHandler();
    }

    render() {
        return <form className="form-inline">
            <button type="submit" className="btn btn-secondary" value="submit" onClick={this.logOut}>Выйти</button>
        </form>;
    }
}
