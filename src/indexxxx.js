import React from "react";
import ReactDOM from "react-dom";
import styles from "./styles/App.css";
import {Sidebar} from "./components/Sidebar";

//import {ItemsList, Item} from "./components/Sidebar";


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            metrics: {},
            hosts: {},
            values: {},
            data: [],
            times: {},
            servers: {},
            targetServer : "server.id1",
            store: [],
            objectKeys: {}
        };
    }

    componentDidMount() {
        var socket = new WebSocket("ws://localhost:8080/sub");
        socket.onmessage = function (message) {
            let metric;
            try {
                const data = message.data;
                metric = JSON.parse(data);

            } catch (error) {
                console.info(error);
                return;
            }

            this.setState((prevState) => {
                const metricsState = prevState.metrics;
                const hostsState = prevState.hosts;
                const valuesState = prevState.values;
                const targetState = prevState.targetServer;
                const timeState = prevState.times;
                const serverState = prevState.servers;
                let dataState = prevState.data;
                let storeState = prevState.store;

                metricsState[metric.metric_name] = metric;
                hostsState[metric.server_name] = metric;

                // if (!isArray(hostsState[metric.server_name])) {
                //     hostsState[metric.server_name] = [];
                // }
                //hostsState[metric.server_name].push(metric);

                valuesState[metric.value] = metric;
                timeState[metric.time] = metric;

                storeState.push(metric);

                // storeState.map(function (objects) {
                //     serverState[objects.server_name]=storeState;
                //     console.log(serverState);
                // })
                // alert(storeState);


                return {
                    metrics: metricsState,
                    data: dataState,
                    values: valuesState,
                    hosts: hostsState,
                    times: timeState,
                    servers: serverState,
                    targetServer: targetState,
                    store: storeState
                };
            });
        }.bind(this);
    }

    updateData = (value) => {
        this.setState({targetServer : value});
        //alert(this.state.targetServer);
    }

    render() {
        //console.log(this.state.targetServer);
        return (
            <React.Fragment>
                <div className="SideHeader">
                    <SiteHeader/>
                </div>
                <div className="container-fluid">
                    <div className="row">
                        <Sidebar
                            hosts={this.state.hosts}
                            metrics={this.state.metrics}
                            values={this.state.values}
                            updateData={this.updateData}
                        />
                        <Chart metrics={this.state.metrics} hosts={this.state.hosts} values={this.state.values}
                               time={this.state.times} store={this.state.store} target={this.state.targetServer}/>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

class SiteHeader extends React.Component {
    render() {
        return <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
            <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="#">IntersvyazMetricSystem</a>
            <form className="form-inline">
                <div className="form-group">
                    <label className="sr-only" htmlFor="exampleInputEmail3">Логин</label>
                    <input type="text" className="form-control" id="exampleInputEmail3" placeholder="Логин"/>
                </div>
                <div className="form-group">
                    <label className="sr-only" htmlFor="exampleInputPassword3">Пароль</label>
                    <input type="password" className="form-control" id="exampleInputPassword3" placeholder="Пароль"/>
                </div>
                <button type="submit" className="btn btn-secondary">Войти</button>
            </form>
        </nav>;
    }
}

class Graphik extends  React.Component {
    constructor(props) {
        super(props);

        this.state = {
            points: []
        };
        this.chart = null;
    }

    componentDidMount() {
        this.chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "График средних значений"
            },
            axisY:{
                includeZero: false
            },
            data: [{
                type: "line",
                dataPoints: [
                    { y: 450 },
                    { y: 414},
                    { y: 520, indexLabel: "highest",markerColor: "red", markerType: "triangle" },
                    { y: 460 },
                    { y: 450 },
                    { y: 500 },
                    { y: 480 },
                    { y: 480 },
                    { y: 410 , indexLabel: "lowest",markerColor: "DarkSlateGrey", markerType: "cross" },
                    { y: 500 },
                    { y: 480 },
                    { y: 510 }
                ]
            }]
        });

        this.forceUpdate();

    }

    render() {
        if (this.chart !== null) {
            this.chart.render();
        }
       // this.chart.dataPoints = this.state.points;
        //this.chart.update();
        return "";
    }
}

class Chart extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const $this=this;
        console.log(this.props.target);
        return <main role="main" className="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div
                className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h1 className="h2">Общая нагрузка на сервер</h1>
                <div className="btn-toolbar mb-2 mb-md-0">
                    <div className="btn-group mr-2">
                        <button className="btn btn-sm btn-outline-secondary">Скачать</button>
                        <button className="btn btn-sm btn-outline-secondary">Поделиться</button>
                    </div>
                    <button className="btn btn-sm btn-outline-secondary dropdown-toggle">
                        <span data-feather="calendar"></span>
                        Эта неделя
                    </button>
                </div>
            </div>

            <div id="chartContainer"></div>

            {
                <Graphik/>
            }

            <h2>Real-time Metric System</h2>
            <div className="table-responsive">
                <table className="table table-striped table-sm">
                    <thead>
                        <tr>
                            <th>Метрика</th>
                            <th>Имя сервера</th>
                            <th>Значение</th>
                            <th>Время</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.store.map(function (element) {
                                if (element.server_name == $this.props.target) {
                                    console.log(1);
                                    return <tr key={element}>
                                        <TableRow element={element}/>
                                    </tr>
                                }
                            })
                        }
                    </tbody>
                </table>
            </div>
        </main>;
    }
}

class TableRow extends React.Component {
    render() {
        return Object.values(this.props.element).map(names => {
            return <td key={names}>{names}</td>;
        });
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById("root")
);

